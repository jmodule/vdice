#!/usr/bin/env python
#$Id: create_gui_ids.py,v 1.1.1.1 2004-03-30 16:13:43 jfriant Exp $
#
# This file is part of the EzGallery program
#
# EzGallery - Create html image galleries to be published on a web server.
# Copyright (C) 2004  Jakim Friant
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# For questions or comments about the software contact: jakim@friant.org

import os
import re

def main():
    obj_out = """#!/usr/bin/env python
# Object ID's are auto-generated from another python script
#
# This file is part of the EzGallery program
# Copyright (C) 2004  Jakim Friant
#

import wx

"""

    #
    # These are the regular expressions for the ID's we're looking for
    #
    re_mnu = re.compile("self\..*\((ID_[A-Z0-9_]*),")
    re_obj = re.compile("self\..* (ID_[A-Z0-9_]*)")

    file_list = os.listdir('.')

    print os.linesep, "Looking for object ID's in the python files:", os.linesep

    total = 0

    for filename in file_list:
        name = filename[:filename.rfind('.')]
        ext = filename[filename.rfind('.')+1:len(filename)]
        if ext == 'py':
            print filename, '...',
            cnt = 0
            f = file(filename, 'r')
            for line in f:
                obj_id = None
                result = re_mnu.search(line)
                if result == None:
                    result = re_obj.search(line)
    #                if result == None:
    #                    result = re_obj2.search(line)
                if result:
                    cnt += 1
                    obj_out += result.group(1) + ' = wx.NewId()'
                    obj_out += '     # ' + filename
                    obj_out += os.linesep

            print "found", cnt, "object ID's"
            total += cnt

    f = file('gui_ids.py', 'w')
    f.write(obj_out)
    f.close()

    print os.linesep, "Found", total, "gui object ID's", os.linesep

if __name__ == "__main__":
    main()
