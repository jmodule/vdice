#!/usr/bin/env python
# Object ID's are auto-generated from another python script
#
# This file is part of the EzGallery program
# Copyright (C) 2004  Jakim Friant
#

import wx

ID_OK = wx.NewId()     # cDlgModifier.py
ID_CANCEL = wx.NewId()     # cDlgModifier.py
ID_NEW = wx.NewId()     # cMain.py
ID_OPEN = wx.NewId()     # cMain.py
ID_SAVE = wx.NewId()     # cMain.py
ID_SAVE_AS = wx.NewId()     # cMain.py
ID_EXIT = wx.NewId()     # cMain.py
ID_ABOUT = wx.NewId()     # cMain.py
ID_D4 = wx.NewId()     # cMain.py
ID_D6 = wx.NewId()     # cMain.py
ID_D8 = wx.NewId()     # cMain.py
ID_D10 = wx.NewId()     # cMain.py
ID_D12 = wx.NewId()     # cMain.py
ID_D20 = wx.NewId()     # cMain.py
ID_D100 = wx.NewId()     # cMain.py
ID_DF = wx.NewId()     # cMain.py
ID_DX = wx.NewId()     # cMain.py
ID_CLEAR_RESULTS = wx.NewId()     # cMain.py
ID_ABOUT_LINK = wx.NewId()     # cAbout.py
ID_ABOUT_CLOSE = wx.NewId()     # cAbout.py
