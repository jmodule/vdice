# generated by wxGlade 0.3.1 on Fri Mar 26 09:49:10 2004

from random import seed, randint
import re
import wx

from cAbout import cAbout
from dicebox import cDiceBox, DEL_ID_START
from cDlgModifier import cDlgModifier
import debug
from gui_ids import *
import icons

# begin wxGlade: dependencies
# end wxGlade

class cMain(wx.Frame):
    #---------------------------------------------------------------------
    def __init__(self, *args, **kwds):
        # begin wxGlade: cMain.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        
        # Menu Bar
        self.main_menubar = wx.MenuBar()
        self.SetMenuBar(self.main_menubar)
        self.mnuFile = wx.Menu()
        self.mnuFile.Append(ID_NEW, "&New\tCtrl+N", "", wx.ITEM_NORMAL)
        self.mnuFile.Append(ID_OPEN, "&Open...\tCtrl+O", "", wx.ITEM_NORMAL)
        self.mnuFile.Append(ID_SAVE, "&Save\tCtrl+S", "", wx.ITEM_NORMAL)
        self.mnuFile.Append(ID_SAVE_AS, "Save &As...", "", wx.ITEM_NORMAL)
        self.mnuFile.AppendSeparator()
        self.mnuFile.Append(ID_EXIT, "E&xit\tCtrl+Q", "", wx.ITEM_NORMAL)
        self.main_menubar.Append(self.mnuFile, "&File")
        self.mnuHelp = wx.Menu()
        self.mnuHelp.Append(ID_ABOUT, "&About\tF1", "", wx.ITEM_NORMAL)
        self.main_menubar.Append(self.mnuHelp, "&Help")
        # Menu Bar end
        self.sb_main = self.CreateStatusBar(1)
        
        # Tool Bar
        self.tb_buttons = wx.ToolBar(self, -1, style=wx.TB_HORIZONTAL|wx.TB_FLAT|wx.TB_TEXT|wx.TB_NOICONS)
        self.SetToolBar(self.tb_buttons)
        self.tb_buttons.AddLabelTool(ID_D4, "D4", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D4", "Add one or more 4-sided dice")
        self.tb_buttons.AddLabelTool(ID_D6, "D6", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D6", "Add one or more 6-sided dice")
        self.tb_buttons.AddLabelTool(ID_D8, "D8", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D8", "Add one or more 8-sided dice")
        self.tb_buttons.AddLabelTool(ID_D10, "D10", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D10", "Add one or more 10-sided dice")
        self.tb_buttons.AddLabelTool(ID_D12, "D12", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D12", "Add one or more 12-sided dice")
        self.tb_buttons.AddLabelTool(ID_D20, "D20", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D20", "Add one or more 20-sided dice")
        self.tb_buttons.AddLabelTool(ID_D100, "D100", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "D100", "Add one or more percentile dice")
        self.tb_buttons.AddLabelTool(ID_DF, "DF", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "DF", "Add one or more FUDGE dice")
        self.tb_buttons.AddLabelTool(ID_DX, "Dx", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, "DX", "Add one or more dice with any number of sides")
        # Tool Bar end
        self.pnl_dice = wx.Panel(self, -1)
        self.txt_results = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE|wx.TE_READONLY)
        self.cmd_clear = wx.Button(self, ID_CLEAR_RESULTS, "Clear")

        self.__set_properties()
        self.__do_layout()
        # end wxGlade
        wx.EVT_MENU(self, ID_NEW, self.OnNew)
        wx.EVT_MENU(self, ID_OPEN, self.OnOpen)
        wx.EVT_MENU(self, ID_SAVE, self.OnSave)
        wx.EVT_MENU(self, ID_SAVE_AS, self.OnSaveAs)
        wx.EVT_MENU(self, ID_EXIT, self.OnExit)
        wx.EVT_MENU(self, ID_ABOUT, self.OnAbout)

        wx.EVT_TOOL(self, ID_D4, self.OnAddDie)
        wx.EVT_TOOL(self, ID_D6, self.OnAddDie)
        wx.EVT_TOOL(self, ID_D8, self.OnAddDie)
        wx.EVT_TOOL(self, ID_D10, self.OnAddDie)
        wx.EVT_TOOL(self, ID_D12, self.OnAddDie)
        wx.EVT_TOOL(self, ID_D20, self.OnAddDie)
        wx.EVT_TOOL(self, ID_D100, self.OnAddDie)
        wx.EVT_TOOL(self, ID_DF, self.OnAddDie)
        wx.EVT_TOOL(self, ID_DX, self.OnAddDie)

        wx.EVT_TOOL_RCLICKED(self, ID_D4, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_D6, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_D8, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_D10, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_D12, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_D20, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_D100, self.OnInstantRoll)
        wx.EVT_TOOL_RCLICKED(self, ID_DF, self.OnInstantRoll)

        wx.EVT_BUTTON(self, ID_CLEAR_RESULTS, self.OnClear)

        seed()

        self.dice_box = cDiceBox()

        self.current_path = ""
        self.needs_save = False

    #---------------------------------------------------------------------
    def __set_properties(self):
        # begin wxGlade: cMain.__set_properties
        self.SetTitle("VDice")
        self.SetSize((392, 481))
        self.sb_main.SetStatusWidths([-1])
        # statusbar fields
        sb_main_fields = ["frame_1_statusbar"]
        for i in range(len(sb_main_fields)):
            self.sb_main.SetStatusText(sb_main_fields[i], i)
        self.tb_buttons.Realize()
        # end wxGlade

    #---------------------------------------------------------------------
    def __do_layout(self):
        # begin wxGlade: cMain.__do_layout
        sizer_1 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_1.Add(self.pnl_dice, 1, wx.EXPAND, 0)
        sizer_2.Add(self.txt_results, 1, wx.EXPAND, 0)
        sizer_2.Add(self.cmd_clear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 4)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)
        self.SetAutoLayout(1)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    #---------------------------------------------------------------------
    def UpdateDice(self):
        """Rebuild the dice controls."""
        sz_all = wx.BoxSizer(wx.VERTICAL)
        for obj in self.dice_box.Objects():
            sz_dice = wx.BoxSizer(wx.HORIZONTAL)

            sz_dice.Add(obj[0], 0, wx.EXPAND, 0)
            sz_dice.Add(obj[1], 0, wx.ALIGN_CENTER_VERTICAL, 0)

            sz_all.Add(sz_dice, 1, wx.EXPAND, 0)

        self.pnl_dice.SetAutoLayout(1)
        self.pnl_dice.SetSizer(sz_all, True)
        self.pnl_dice.Layout()
        self.pnl_dice.Update()

    #---------------------------------------------------------------------
    def OnAddDie(self, event):
        """Event handler to add a die from the toolbar."""

        id = event.GetId()
        type = self.tb_buttons.GetToolShortHelp(id)
        (number, sides, modifier) = self.GetOptions(type)
        if number != 0:
            self.dice_box.AddRow(number + 'D' + sides + modifier, self)
            self.needs_save = True
            self.UpdateDice()

    #---------------------------------------------------------------------
    def OnDel(self, event):
        """Handle the clear row event"""

        which = event.GetId() - DEL_ID_START
        debug.report("Notice", "OnClear", "cmd id", which)
        self.dice_box.DelRow(which)
        self.needs_save = True
        self.UpdateDice()

    #---------------------------------------------------------------------
    def OnClear(self, event):
        """Clear the results text box."""
        self.txt_results.Clear()

    #---------------------------------------------------------------------
    def OnNew(self, event):
        """Delete all the dice buttons and start over."""
        #if self.needs_save:
        #    wxMessageDlg()
        self.dice_box.DelAll()
        self.UpdateDice()
        self.needs_save = False

    #---------------------------------------------------------------------
    def OnOpen(self, event):
        """Open a file."""
        filter = "XML Files (*.xml)|*.xml|All Files|*.*"
        dialog = wx.FileDialog(self, 'Open', self.current_path, "", filter)
        if dialog.ShowModal() == wx.ID_OK:
            self.current_path = dialog.GetPath()
            self.dice_box.Load(self.current_path, self)
            self.UpdateDice()

    #---------------------------------------------------------------------
    def OnSave(self, event):
        """Save the current file."""
        if self.current_path == "":
            self.OnSaveAs(event)
        else:
            self.dice_box.Save(self.current_path)
            self.needs_save = False

    #---------------------------------------------------------------------
    def OnSaveAs(self, event):
        """Choose and save the file."""
        path = ""
        filename = self.current_path
        wildcard = "*.xml"
        dialog = wx.FileDialog(self, 'Save Project', path, filename, wildcard, wx.SAVE | wx.OVERWRITE_PROMPT | wx.CHANGE_DIR)
        if dialog.ShowModal() == wx.ID_OK:
            self.current_path = dialog.GetPath()
            self.dice_box.Save(self.current_path)
            self.needs_save = false
     
    #---------------------------------------------------------------------
    def OnExit(self, event):
        """Exit the application."""
        self.Close(False)

    #---------------------------------------------------------------------
    def OnRoll(self, event):
        """Handle the roll dice event"""
        die_code = event.GetEventObject().GetLabel()
        (number, sides, modifier) = self.ParseDieCode(die_code)

        debug.report("Notice", "OnRoll", "number, sides, modifier", str(number) + ',' + str(sides) + ',' + str(modifier))
        self.DoRoll(number, sides, modifier)

    #---------------------------------------------------------------------
    def DoRoll(self, number, sides, modifier):
        """Display a die roll in the result text box."""
        roll = 0
        for i in range(int(number)):
            roll += randint(1, int(sides))
        roll += int(modifier)

        result = str(number) + 'd' + str(sides)
        if modifier != 0:
            result += str(modifier)
        result += ' = ' + str(roll)
        self.txt_results.AppendText(result + '\n')

    #---------------------------------------------------------------------
    def ParseDieCode(self, die_code):
        """Return a tuple of the number of dice, sides, and modifier."""
        modifier = 0
        number = 1
        sides = 6

        re_num = re.compile("([1-9][1-9]*)D")
        re_mod = re.compile("D[0-9]*([\-|\+]\d+)")
        re_side = re.compile("D([1-9][0-9]*)")
        re_fudge = re.compile("D[fF]")
        
        # find the number of dice to roll
        #
        result = re_num.search(die_code)
        if result:
            number = result.group(1)

        # find any modifiers
        #
        result = re_mod.search(die_code)
        if result:
            modifier = result.group(1)

        # find the number of sides
        #
        result = re_side.search(die_code)
        if result:
            sides = result.group(1)
        else:
            result = re_fudge.search(die_code)
            if result:
                sides = 3
                modifier = -2 * int(number)

        return (number, sides, modifier)

    #---------------------------------------------------------------------
    def OnInstantRoll(self, event):
        """Produce a die roll when the toolbar is right-clicked."""
        id = event.GetId()
        die_code = self.tb_buttons.GetToolShortHelp(id)
        (number, sides, modifier) = self.ParseDieCode(die_code)
        self.DoRoll(number, sides, modifier)

    #---------------------------------------------------------------------
    def GetOptions(self, type):
        """Return the number of dice, sides, and modifier as a tuple."""
        number = 0
        sides = 0
        modifier = 0

        if type.upper() == 'DF':
            number = '4'
            sides = '3'
            modifier = '-8'
        else:
            dlg = cDlgModifier(self, -1, '')

            sides = type[1:]
            if sides.lower() == 'x':
                dlg.CanSetSides()
                sides = 1
            dlg.spn_sides.SetValue(int(sides))

            if dlg.ShowModal() == wx.ID_OK:
                number = str(dlg.spn_number.GetValue())
                sides = str(dlg.spn_sides.GetValue())
                mod = dlg.spn_modifier.GetValue()
                if mod == 0:
                    modifier = ""
                elif mod > 0:
                    modifier = "+" + str(mod)
                else:
                    modifier = str(mod)

        return (number, sides, modifier)

    #---------------------------------------------------------------------
    def OnAbout(self, event):
        """Display the about dialog."""
        dlg = cAbout(self, -1, '')
        dlg.ShowModal()

# end of class cMain


