#!/usr/bin/env python

from os import linesep
import wx
import xml.parsers.expat

import debug
import icons

DICE_ID_START = 1000
DEL_ID_START = 1500

class cDiceBox:
    """Holds the dice button row information.

    Has functions to load and save various configurations."""

    #---------------------------------------------------------------------
    def __init__(self):
        """Initialize the class"""
        self.rows = dict()
        self.cnt = 0
        self.curr_key = ""
        self.curr_tag = ""
        self.curr_data = ""
        self.parent = None

    #---------------------------------------------------------------------
    def Objects(self):
        return self.rows.values()

    #---------------------------------------------------------------------
    def AddRow(self, name, parent):
        """Return a h-sizer with a die button, result box, and clear button."""
        sz_dice = None
        if len(self.rows.keys()) < 10:
            die_id = DICE_ID_START + self.cnt
            del_id = DEL_ID_START + self.cnt
            self.rows[self.cnt] = (wx.Button(parent, die_id, str(name)),
                       wx.BitmapButton(parent, del_id, icons.getDeleteBitmap()))
            wx.EVT_BUTTON(parent, die_id, parent.OnRoll)
            wx.EVT_BUTTON(parent, del_id, parent.OnDel)
            self.cnt += 1


    #---------------------------------------------------------------------
    def DelRow(self, which):
        self.rows[which][0].Hide()
        self.rows[which][1].Hide()
        del self.rows[which]

    #---------------------------------------------------------------------
    def DelAll(self):
        all_keys = self.rows.keys()
        for key in all_keys:
            self.DelRow(key)

    #---------------------------------------------------------------------
    def Save(self, path_to_file):
        """Save the button information to an XML file."""
        # check to make sure the file has an extension (this may be
        # redundant if turns out that the file dialog does this)
        #
        ext = path_to_file[path_to_file.rfind('.')+1:len(path_to_file)]
        debug.report('Notice', 'Save', 'file extension', ext)
        if ext == '':
            path_to_file += '.xml'
        # open file for writing
        #
        outfile = file(path_to_file, 'w')
        outfile.write(self.AsXml())
        outfile.close()

    #---------------------------------------------------------------------
    def Load(self, path_to_file, parent):
        """Read the dice data from a file."""

        # we need the gui parent window so we can build the buttons
        #
        self.parent = parent
        xml_file = file(path_to_file, 'r')
        p = xml.parsers.expat.ParserCreate()

        p.StartElementHandler = self.StartElement
        p.EndElementHandler = self.EndElement
        p.CharacterDataHandler = self.CharData

        # try:
        p.ParseFile(xml_file)

    #---------------------------------------------------------------------
    def StartElement(self, name, attrs):
        debug.report("Notice", "StartElement", "name, attrs", name, attrs)
        self.curr_tag = name
        if name == 'row':
            self.curr_key = attrs['key']

    #---------------------------------------------------------------------
    def EndElement(self, name):
        debug.report("Notice", "EndElement", "name", name)
        self.curr_tag = ""

    #---------------------------------------------------------------------
    def CharData(self, data):
        debug.report("Notice", "CharData", "data", data)
        if self.curr_tag == 'row':
            self.AddRow(data, self.parent)

    #---------------------------------------------------------------------
    def AsXml(self):
        """Return the dice button info in XML format."""

        xml_out = '<?xml version="1.0" encoding="ISO-8859-1"?>' + linesep
        xml_out += '<dicebox>' + linesep
        for key in self.rows.keys():
            xml_out += '    <row key="' + str(key) + '">' + \
                       self.rows[key][0].GetLabel() + \
                       '</row>' + linesep
        xml_out += '</dicebox>' + linesep

        return xml_out

