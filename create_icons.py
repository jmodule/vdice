#!/usr/bin/env python
#$Id: create_icons.py,v 1.1 2004-03-30 21:54:45 jfriant Exp $
#
# This file is part of the EzGallery program
#
# EzGallery - Create html image galleries to be published on a web server.
# Copyright (C) 2004  Jakim Friant
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# For questions or comments about the software contact: jakim@friant.org

import os

def main():
    icons_hdr = """#!/usr/bin/env python
#
# These icons are converted from XPMs and imbeded here by img2py
#
# This file is part of the EzGallery program
# Copyright (C) 2004  Jakim Friant
#

from wx import BitmapFromImage as wxBitmapFromImage, ImageFromStream as wxImageFromStream
import cStringIO
import zlib

"""

    f = file('icons.py', 'w')
    f.write(icons_hdr)
    f.close()

    file_list = os.listdir('ico')

    for img in file_list:
        name = img[:img.rfind('.')]
        ext = img[img.rfind('.')+1:len(img)]
        if ext == 'xpm':
            os.system('img2py -n ' + name + ' -a ico/' + img + ' icons.py')

    print 'Done'

if __name__ == "__main__":
    main()
