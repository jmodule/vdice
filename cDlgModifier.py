# generated by wx.Glade 0.3.1 on Tue Mar 30 15:57:18 2004

import wx

from gui_ids import *

# begin wx.Glade: dependencies
# end wx.Glade

class cDlgModifier(wx.Dialog):

    #---------------------------------------------------------------------
    def __init__(self, *args, **kwds):
        # begin wx.Glade: cDlgModifier.__init__
        kwds["style"] = wx.DIALOG_MODALITY_WINDOW_MODAL|wx.CAPTION
        wx.Dialog.__init__(self, *args, **kwds)
        self.lbl_message = wx.StaticText(self, -1, "Enter the number of dice and any modifier.")
        self.lbl_prompt_dice = wx.StaticText(self, -1, "Number of Dice")
        self.spn_number = wx.SpinCtrl(self, -1, "1", min=1, max=100, style=wx.SP_ARROW_KEYS)
        self.lbl_prompt_sides = wx.StaticText(self, -1, "Number of Sides")
        self.spn_sides = wx.SpinCtrl(self, -1, "0", min=1, max=999, style=wx.SP_ARROW_KEYS)
        self.lbl_prompt_modifier = wx.StaticText(self, -1, "Modifier")
        self.spn_modifier = wx.SpinCtrl(self, -1, "0", min=-100, max=100, style=wx.SP_ARROW_KEYS)
        self.cmd_ok = wx.Button(self, ID_OK, "OK")
        self.cmd_cancel = wx.Button(self, ID_CANCEL, "Cancel")

        self.__set_properties()
        self.__do_layout()
        # end wx.Glade
        wx.EVT_BUTTON(self, ID_OK, self.OnOk)
        wx.EVT_BUTTON(self, ID_CANCEL, self.OnCancel)

    #---------------------------------------------------------------------
    def __set_properties(self):
        # begin wx.Glade: cDlgModifier.__set_properties
        self.SetTitle("Die Roll Modifier")
        self.spn_number.SetToolTipString("Enter a number of dice from 1 to 100")
        self.lbl_prompt_sides.Enable(0)
        self.spn_sides.Enable(0)
        self.spn_modifier.SetToolTipString("Enter a modifier from -100 to 100")
        self.cmd_ok.SetDefault()
        # end wx.Glade

    #---------------------------------------------------------------------
    def __do_layout(self):
        # begin wx.Glade: cDlgModifier.__do_layout
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3.Add(self.lbl_message, 0, wx.ALL, 8)
        sizer_4.Add(self.lbl_prompt_dice, 1, 0, 0)
        sizer_4.Add(self.spn_number, 1, 0, 8)
        sizer_3.Add(sizer_4, 0, wx.ALL|wx.EXPAND, 8)
        sizer_6.Add(self.lbl_prompt_sides, 1, 0, 0)
        sizer_6.Add(self.spn_sides, 1, 0, 0)
        sizer_3.Add(sizer_6, 0, wx.ALL|wx.EXPAND, 8)
        sizer_5.Add(self.lbl_prompt_modifier, 1, wx.ALIGN_CENTER_VERTICAL, 16)
        sizer_5.Add(self.spn_modifier, 1, 0, 8)
        sizer_3.Add(sizer_5, 0, wx.ALL|wx.EXPAND, 8)
        sizer_7.Add(self.cmd_ok, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 8)
        sizer_7.Add(self.cmd_cancel, 0, wx.ALL, 8)
        sizer_3.Add(sizer_7, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        self.SetAutoLayout(1)
        self.SetSizer(sizer_3)
        sizer_3.Fit(self)
        sizer_3.SetSizeHints(self)
        self.Layout()
        self.Centre()
        # end wx.Glade

    #---------------------------------------------------------------------
    def OnOk(self, event):
        """Handle the OK button event."""
        self.EndModal(wx.ID_OK)

    #---------------------------------------------------------------------
    def OnCancel(self, event):
        """Handle the Cancel button event."""
        self.EndModal(wx.ID_CANCEL)

    #---------------------------------------------------------------------
    def CanSetSides(self):
        """Allow the user to pick the number of sides."""
        self.lbl_prompt_sides.Enable(True)
        self.spn_sides.Enable(True)
        self.lbl_message.SetLabel("Enter the number of dice, sides, and any modifier.")

# end of class cDlgModifier


