# NAME

Virtual Dice (vDice.py)

# PURPOSE

Create buttons to represent different die types and then use them
to "roll" dice.  All the results are kept in a text box until cleared
by the user.  Dice button combinations can be saved in an XML format
and reloaded at a later time.
 
# REQUIREMENTS

This program uses Python and wxPython.

# AUTHOR

J. Friant <jfriant@gmail.com>