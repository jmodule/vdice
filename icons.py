#!/usr/bin/env python
#
# These icons are converted from XPMs and imbeded here by img2py
#
# This file is part of the EzGallery program
# Copyright (C) 2004  Jakim Friant
#

from wx import BitmapFromImage as wxBitmapFromImage, ImageFromStream as wxImageFromStream
import cStringIO
import zlib

#----------------------------------------------------------------------
def getDeleteData():
    return zlib.decompress(
'x\xda\xeb\x0c\xf0s\xe7\xe5\x92\xe2b``\xe0\xf5\xf4p\t\x02\xd2\x02 \xcc\xc1\
\x06$\xe5?\xffO\x04R,\xc5N\x9e!\x1c@P\xc3\x91\xd2\x01\xe4\'x\xba8\x86T\xccY;\
\xd9\x9a\xef\x80\x01\x87s\xfc\x89\xbf\xdf\xe5\xa6\xf6\xfaM\x8d\xfa\x9a\x1c\
\xa2.\xe4\xdcR\x98\xc4\xac\x12\xe2\x94\x1b\xa0\xac6\xd7"K\xe8U\xd1\xcb\ts>\
\x9a3\xb9~j\xa9\xf5-\xe9r\xf7Qqx\xfaD\xfbUy\xd0\xf2I\xd7\x8c^\x9f\xbes\xc0\
\xd6[5B-*w\x0b\xdbI\xb9\xa7\xa9\xa7\xbf\x94\xcf-\xb7\xfd\xf03u\xd7\xbb\xdd2@\
\xbb\x18<]\xfd\\\xd69%4\x01\x00i\xf3<[' )

def getDeleteBitmap():
    return wxBitmapFromImage(getDeleteImage())

def getDeleteImage():
    stream = cStringIO.StringIO(getDeleteData())
    return wxImageFromStream(stream)

