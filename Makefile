.PHONY: all clean patch gui

all:
	python vdice.py

clean:
	-rmb
	-rm *.pyc

patch:
	python create_gui_ids.py

gui:
	wxglade vdice.wxg
