#!/usr/bin/env python
#$Id: debug.py,v 1.4 2004-04-01 21:02:30 jfriant Exp $
#
# This file is part of the VDice program
#
# VDice - The virtual dice program.
# Copyright (C) 2004  Jakim Friant
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# For questions or comments about the software contact: jakim@friant.org

# Note: This was borrowed from my t2kdb project

# The debug level is from 0 to 3
# Level 0 will display only emergency messages (very few of these)
# Level 1 will display full errors, for basic debugging
# Level 2 will display warnings too, for full debugging
# Level 3 will display all notices, warnings, and errors
#
# The default debug level is 1

error_levels = { 'None' : 0,
                 'Error' : 1,
                 'Warning' : 2,
                 'Notice' : 3,
                 'Debug' : 4 }

level = error_levels['Error']

#------------------------------------------------------------------------
def report(msg_level, function, summary, *arguments):
    """Used to create a standard error message report for debugging.

    msg_level  - a string indicating at what level the message should be
                 displayed
    function   - the function name
    summary    - the error message
    *arguments - for futher debugging, the variables that caused the error
    """

    #msg = '[' + time.asctime(time.localtime(time.time())) + '] '
    msg = ""

    if error_levels.has_key(msg_level):
        priority = error_levels[msg_level]
    else:
        priority = 1

    if level >= priority:
        msg += msg_level + ': ' + summary
        msg += ' in ' + function + '()'
        for arg in arguments:
            msg += ', ' + str(arg)
        print msg

def AtLeast(check_level):
    """Check to see if debug level is at least equal to the level passed in.
    
    Actually the check is backwards from what the name implies since the debug
    level is the inverse of the when-to-report level (or something like that).
    """
    ans = False
    if error_levels.has_key(check_level):
        if error_levels[check_level] < level:
            ans = True
    return ans
